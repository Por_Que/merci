﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Vector2 movement;
    private float movementSpeed;

    [Header("Character attributes:")]
    public float MOVEMENT_BASE_SPEED = 5f;    

    [Space]
    [Header("References")]
    public Rigidbody2D rb;
    public Animator animator;

    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        movementSpeed = Mathf.Clamp(movement.magnitude, 0.0f, 1.0f);
    }
    void FixedUpdate()
    {        
        Move();
        Animate();
    }    

    void Move()
    {
        rb.MovePosition(rb.velocity = rb.position + movement * MOVEMENT_BASE_SPEED * Time.fixedDeltaTime);
    }

    void Animate()
    {
        if (movement != Vector2.zero)
        {
            animator.SetFloat("Horizontal", movement.x);
            animator.SetFloat("Vertical", movement.y);
        }
        animator.SetFloat("Speed", movementSpeed);

    }
}
